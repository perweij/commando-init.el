;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;   Commando init.el - for copy&paste deploy when visiting new servers  ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Improve defaults
(setq custom-file "~/.emacs.d/custom.el")
;(load custom-file)


;; Setup repos
(require 'package)
;(add-to-list 'package-archives
;             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("nongnu" . "https://elpa.nongnu.org/nongnu/"))
;(add-to-list 'package-archives
;             '("melpa-stable" . "http://stable.melpa.org/packages/") t)


;; Dynamically install package, refreshing when needed
(defun pw/ensure-pkgs (req-pkgs)
  (let* ((inst-pkgs (mapcar (lambda (pkg)
			      (car pkg))
			    package-alist)))
    (mapcar (lambda (pkg)
	    (unless (package-installed-p pkg)
	      (unless (member pkg inst-pkgs)
		(package-refresh-contents))
	      (package-install pkg)))
	  req-pkgs)))

(pw/ensure-pkgs (list 'autorevert
                      'cider
                      'circe
                      'editorconfig
                      'forge
		      'git-timemachine
		      'gruvbox-theme
		      'markdown-mode
		      'org-ai
		      'org-super-agenda
		      'project-explorer
		      'projectile
		      'request
		      'shell-pop
		      'undo-tree
		      'use-package
		      'which-key
                      'sqlite3
                      'llm
                      ))





;; improve UI
(add-hook 'before-save-hook
          'delete-trailing-whitespace)
(setq warning-suppress-log-types '((comp)))

;; General emacs settings
(use-package emacs
  :ensure nil
  :init
  (put 'narrow-to-region 'disabled nil)
  (put 'downcase-region 'disabled nil)
  (put 'upcase-region 'disabled nil)
  :custom
  (scroll-step 1)
  (inhibit-startup-screen t "Don't show splash screen")
  (use-dialog-box nil "Disable dialog boxes")
  (enable-recursive-minibuffers t "Allow minibuffer commands in the minibuffer")
  (indent-tabs-mode nil "Spaces!")
  (debug-on-quit nil)
  :config
  (setq-default indent-tabs-mode nil)
  (setq-default line-number-mode t)
  (setq-default column-number-mode t)
  (setq display-time-day-and-date t
        display-time-24hr-format t)
  (setq echo-keystrokes 0.02)
  (setq default-major-mode 'fundamental-mode)
  (setq visible-bell t)
  (setq-default search-highlight t)
  ;; isearch anvander highlight!
  (copy-face 'highlight 'isearch)
  (copy-face 'highlight 'region)
  ;; Turn off truncate
  (setq truncate-partial-width-windows nil)
  (setq window-min-height 3)
  (setq track-eol t)
  (setq mouse-yank-at-point t)
  (setq print-escape-newlines t)
  (setq garbage-collection-messages nil)
  (setq compilation-scroll-output t)
  ;; customize hexl
  (setq hexl-follow-ascii t)
  (setq hexl-iso "-iso")
  ;; Turn off secondary selection
  (setq global-unset-key [M-mouse-1])
  (setq global-unset-key [M-drag-mouse-1])
  (setq global-unset-key [M-down-mouse-1])
  (setq global-unset-key [M-mouse-3])
  (setq global-unset-key [M-mouse-2])
  ;; skip outdated, compiled files
  (setq load-prefer-newer t)
  ;; other stuff
  (display-time)
  (midnight-mode t)
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (blink-cursor-mode -1)
  (transient-mark-mode 1)
  ;;(column-number-mode t) - see above
  ;; dark theme with decent contrast
  (load-theme 'deeper-blue t))



(defun load-http-file (url)
  "Retrieve URL if not already retrieved, store in user-emacs-directory, then load it."
  (let ((local-file
	 (expand-file-name (file-name-nondirectory url) user-emacs-directory)))
    (unless (file-exists-p local-file)
      (shell-command (concat "curl -L -o " local-file " " url)))
    (load local-file nil 'nomessage)))


;; load unpackaged stuff
(load-http-file "https://web.archive.org/web/20160312052336/http://www.northbound-train.com/emacs-hosted/backup-dir.el")
(load-http-file "https://raw.githubusercontent.com/camdez/goto-last-change.el/master/goto-last-change.el")



;; backups
(require 'backup-dir)
(make-variable-buffer-local 'backup-inhibited)
(setq bkup-backup-directory-info
      '((t "~/.saves" ok-create full-path prepend-name)))
(setq delete-old-versions t
      kept-old-versions 1
      kept-new-versions 3
      version-control t)

;; autorevert
(use-package autorevert
  :ensure nil
  :diminish auto-revert-mode
  :config
  (global-auto-revert-mode t))


;; Git
(require 'magit)
(global-set-key (kbd "C-x m") 'magit-status)
(global-set-key (kbd "C-c g") 'magit-file-dispatch)
(require 'git-timemachine)
(require 'forge)


;; magit-gptcommit
(require 'auth-source)
(defun get-password (host user)
  "Retrieve the password for the given HOST and USER from ~/.authinfo."
  (let ((auth-info (car (auth-source-search :host host
                                            :user user
                                            :require '(:user :secret)))))
    (if auth-info
        (let ((secret (plist-get auth-info :secret)))
          (if (functionp secret)
              (funcall secret)
            secret))
      (error "No auth info found for %s with user %s" host user))))

(require 'llm)
(require 'llm-openai)

;; DO THIS FIRST: wget https://github.com/avishefi/magit-gptcommit/raw/llm/magit-gptcommit.el
(load-file "/home/perweij/proj/commando-init.el/magit-gptcommit.el")

;; (require 'magit-gptcommit)
;; (setq magit-gptcommit-llm-provider
;;       (make-llm-openai :key (get-password "api.openai.com" "apikey")
;; 		       :chat-model "o1-mini"))
;; (magit-gptcommit-mode 1)
;;

(use-package magit-gptcommit
  :demand t
  :after magit
  :bind (:map git-commit-mode-map
              ("C-c C-g" . magit-gptcommit-commit-accept))
  :custom
  (magit-gptcommit-llm-provider (make-llm-openai :key (get-password "api.openai.com" "apikey")
                                                 :chat-model "o1-mini"))

  :config
  ;; Enable magit-gptcommit-mode to watch staged changes and generate commit message automatically in magit status buffer
  ;; This mode is optional, you can also use `magit-gptcommit-generate' to generate commit message manually
  ;; `magit-gptcommit-generate' should only execute on magit status buffer currently
  ;; (magit-gptcommit-mode 1)

  ;; Add gptcommit transient commands to `magit-commit'
  ;; Eval (transient-remove-suffix 'magit-commit '(1 -1)) to remove gptcommit transient commands
  (magit-gptcommit-status-buffer-setup))
(setq llm-warn-on-nonfree nil)
(magit-gptcommit-mode 1)


;; Projectile
(require 'projectile)
(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)


;; which-key
(require 'which-key)
(which-key-mode)


;; windmove: move point from window to window using Shift and the arrow keys.
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))


;; goto-last-change
(global-set-key (kbd "M-u") 'goto-last-change)


;; undo-tree
;;; THIS LINE CAUSES PDUMPER LOADER TO CRASH:
(unless (getenv "SKIP_PDUMPER_BUGS")
  (global-undo-tree-mode))

(with-eval-after-load 'undo-tree
  (setq undo-tree-auto-save-history nil))


;; cider
(unless (package-installed-p 'cider)
  (package-install 'cider))


;; Functions
(defun f-read-text (path &optional coding)
  "Read text with PATH, using CODING.
CODING defaults to `utf-8'.
Return the decoded text as multibyte string."
  (decode-coding-string (f-read-bytes path) (or coding 'utf-8)))


(defun f-read-bytes (path)
  "Read binary data from PATH.
Return the binary data as unibyte string."
  (with-temp-buffer
    (set-buffer-multibyte nil)
    (setq buffer-file-coding-system 'binary)
    (insert-file-contents-literally path)
    (buffer-substring-no-properties (point-min) (point-max))))



(defun util--dump-load-path ()
  (with-temp-buffer
    (insert (prin1-to-string `(setq load-path ',load-path)))
    (fill-region (point-min) (point-max))
    (write-file "~/.emacs.d/load-path.el")))

;; load with emacs -q --dump-file ~/.emacs.d/emacs.dump
(defun my-dump-emacs ()
  "Dump current Emacs config."
  (interactive)
  (util--dump-load-path)
  (shell-command (format "env SKIP_PDUMPER_BUGS=1 emacs --batch -u %s -l ~/.emacs.d/load-path.el -l ~/.emacs.d/init.el -l ~/proj/commando-init.el/dashboard.el --eval '(dump-emacs-portable \"~/.emacs.d/emacs.dump\")'" user-login-name)))



(use-package org-ai
  :ensure t
  :commands (org-ai-mode
             org-ai-global-mode)
  :init
  (add-hook 'org-mode-hook #'org-ai-mode) ; enable org-ai in org-mode
  (org-ai-global-mode) ; installs global keybindings on C-c M-a
  :config
  (setq org-ai-default-chat-model "o1-mini")
  (org-ai-install-yasnippets) ; if you are using yasnippet and want `ai` snippets
  (setq org-ai-openai-api-token (get-password "api.openai.com" "apikey"))
)

(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))

;; make windmove work in org-mode
(defun my-org-hook ()
  (add-hook 'org-shiftup-final-hook 'windmove-up)
  (add-hook 'org-shiftleft-final-hook 'windmove-left)
  (add-hook 'org-shiftdown-final-hook 'windmove-down)
  (add-hook 'org-shiftright-final-hook 'windmove-right))

(eval-after-load 'org
  '(my-org-hook))


(if (or (file-directory-p "~/blandat-jobb")
        (file-directory-p "~/proj-af"))
    (setq pw/environment 'work)
  (setq pw/environment 'private))
