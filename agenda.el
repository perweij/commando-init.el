(defun pw/init-agenda()
  (org-config)
  (use-package org-super-agenda
    :config (org-super-agenda-mode))

  (let* ((entry (car (seq-filter (lambda (elt) (equal "x" (car elt))) org-agenda-custom-commands))))
    (org-agenda-run-series (nth 1 entry) (cddr entry)))
  (switch-to-buffer "*Org Agenda*")
  (other-window 1)
  (find-file (concat org-directory "/cheatsheets_" (system-name) ".org"))
  (other-window 1))


(defun org-config ()
  (if (and (boundp 'pw/environment)
           (eq pw/environment 'work))
      (setq org-directory "~/blandat-jobb/org")
    (setq org-directory "~/proj-bw/org"))
  (defvar org-journal-file (concat org-directory "/journal_" (system-name) ".org")
    "Path to OrgMode journal file.")
  (defvar org-task-file (concat org-directory "/gtd/inbox_" (system-name) ".org"))
  (setq org-default-notes-file org-journal-file)
  (setq org-contacts-files (list (concat org-directory "/contacts_" (system-name) ".org")))

  (require 'org-habit)
  (jv-org-priorities)
  (global-set-key (kbd "C-c l") #'org-store-link)
  (global-set-key (kbd "C-c a") #'org-agenda)
  (global-set-key (kbd "C-c c") #'org-capture)
  (setq org-habit-graph-column 80)
  (setq org-adapt-indentation nil) ;;inhibit annoying indentation behaviour
  (setq org-element-use-cache nil)
  ;;(setq org-log-done 'time) ;; add extra Closed information, but it's redundant
  (setq pw/org-agenda-keywords '("TODO" "WAIT" "DONE" "CANCELED")) ; the raw keywords for matching
  (setq pw/org-agenda-tags '("TIME")) ; the properties for matching
  (setq org-todo-keywords
        '((sequence "TODO(t)" "WAIT(w@/!)" "|" "DONE(d!)" "CANCELED(c@)")))
  (setq org-use-speed-commands t)
  (setq org-confirm-babel-evaluate nil)
  (jv-org-agenda-files)
  (setq org-refile-use-outline-path 'file)
  (setq org-outline-path-complete-in-steps nil)
  (setq org-agenda-include-diary nil) ;; no international holidays etc
  (setq org-super-agenda-unmatched-order 0) ;;put tasks with no prio cookie 1st
  (setq org-default-priority ?0)
  (setq org-treat-insert-todo-heading-as-state-change t)
  (setq org-agenda-span 8)
  (setq org-agenda-start-day "-7d")
  (setq org-agenda-start-with-log-mode 'closed)
  (setq org-agenda-log-mode-items
        '(state clock closed))
  (setq org-super-agenda-unmatched-order 99)

  (setq org-agenda-custom-commands
        pw/agenda-custom-commands)

  (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
  (setq org-capture-templates
        '(("n" "Notes" entry (file+datetree org-journal-file)
           "* %^{Description} %?\nAdded: %U\n\n")

          ("t" "Todo" entry (file+headline org-task-file "Tasks")
           "* TODO %? %^g\n- State \"TODO\" created %U\n")

          ("l" "Log Time" entry (file+datetree org-journal-file )
           "* %U - %^{Activity}  :TIME:")))
  (setq org-refile-targets
	'((nil :maxlevel . 3)
          (org-agenda-files :maxlevel . 3)))

  ;; FIXME: this doesn't seem to work
  (setq org-agenda-prefix-format
        '((agenda . " %i %-20:c%?-12t% s")
          (timeline . "  % s")
          (todo . " %i %3(jv-todoinfo)| %-20:c")
          (tags . " %i %-12:c")
          (search . " %i %-12:c")))
  )



(defun jv-org-agenda-files ()
  (interactive)
  (setq org-agenda-files
        (remove ""
                (mapcar 'abbreviate-file-name
                        (split-string
                         (shell-command-to-string
			  (concat "find "
				  org-directory
;;				  " -type f -regex '.*org' -and -not -name '#*' -exec grep -E --files-with-matches '\\* ("
				  " -type f -regex '.*org' -and -not -name '#*' -exec grep -E --files-with-matches '\* ("
				  (concat "(" (string-join pw/org-agenda-keywords "|") ")")
				  "|.*"
                                  (concat "(" (string-join pw/org-agenda-tags "|") ")")
                                  ")' '{}' ';' " "\n")))))))


;;; org-todo priorities
(defun jv-org-priorities ()
  (setq org-highest-priority ?A ;; 64 @ 48 0, bugs start happening if you have higher prios tnan 0, like '!'
        org-lowest-priority  ?F ;;
        org-default-priority ?C ;;
        org-priority-regexp ".*?\\(\\[#\\([;:<=>?@A-F]\\)\\] ?\\)"))


(defun jv-todoinfo()
  (let
      ((numchilds 0))

    (save-mark-and-excursion
      (org-narrow-to-subtree)
      (setq numchilds (- (length
                          (--filter (eq 'todo it)
                                    (progn
                                      (let ((parsetree (org-element-parse-buffer 'headline)))
					(org-element-map parsetree 'headline
					  (lambda (hl) (org-element-property :todo-type hl))))))) 1))

      ;;im not sure why the widen is needed, but otherwise agenda generating breaks
      (setq numchilds (if (= 0 numchilds) "" numchilds))
      (widen))
    numchilds))


(unless (getenv "SKIP_PDUMPER_BUGS")
  (pw/init-agenda))


(setq pw/agenda-custom-commands
      '(
        ("x" "Tasks, agenda and log"
         ((agenda "" ())
          (tags "TIME")
          (alltodo ""
                   ((org-agenda-overriding-header "")
                    (org-super-agenda-groups
                     '(
                       ;;(:name "Next to do" :todo "NEXT" :order 1)
                       ;;(:name "Important" :tag "Important" :priority "A" :order 2)
                       ;;(:name "Due Today" :deadline today :order 3)
                       ;;(:name "Due Soon" :deadline future :order 4)
                       ;;(:name "Overdue" :deadline past :order 5)
                       ;;(:name "Habits" :tag
                       ;;       ("habits")
                       ;;       :order 7)

                       ;; (:name "" :tag "" :order 9)

                       ;; jobbprojekt
                       (:name "ci" :tag "ci" :order 9)
                       (:name "crm" :tag "crm" :order 9)
                       (:name "djs" :tag "djs" :order 9)
                       (:name "sec" :tag "sec" :order 9)
                       (:name "fragedrivnauppdrag" :tag "fragedrivnauppdrag" :order 9)
                       (:name "ghorg" :tag "ghorg" :order 9)
                       (:name "gitcircus" :tag "gitcircus" :order 9)
                       (:name "gitlabbackup" :tag "gitlabbackup" :order 9)
                       (:name "joblinks" :tag "joblinks" :order 9)
                       (:name "joblinksalert" :tag "joblinksalert" :order 9)
                       (:name "launchotron" :tag "launchotron" :order 9)
                       (:name "mpr" :tag "mpr" :order 9)
                       (:name "nexus" :tag "nexus" :order 9)
                       (:name "ssykgpt" :tag "ssykgpt" :order 9)
                       (:name "mindhammer" :tag "mindhammer" :order 9)





                       ;; Idesamlare
                       (:name "ideacollection" :tag "ideacollection" :order 10)
                       (:name "career" :tag "career" :order 10)

                       ;; hobby
                       (:name "selforg" :tag "selforg" :order 11)
                       (:name "sidcontrol" :tag "sidcontrol" :order 11)
                       (:name "Hem" :tag "hem" :order 11)
                       (:name "Bil" :tag "bil" :order 12)
                       (:name "Emacs" :tag "emacs" :order 13)
                       (:name "C64" :tag "c64" :order 14)
                       (:name "amiga" :tag "amiga" :order 15)
                       (:name "demos" :tag "demos" :order 16)

                       (:name "Dashboard" :tag "dashboard" :order 18)
                       (:name "Waiting" :todo "WAITING" :order 20)
                       (:name "not sure"
                              :order 90)
                       ;;(:discard
                       ;; (:tag
                       ;;  ("Chore" "Routine" "Daily")))
                       ))))))))
